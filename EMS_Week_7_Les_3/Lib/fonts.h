/*
 * fonts.h
 *
 *  Created on: 7 mrt. 2018
 *      Author: VersD
 */

#ifndef FONTS_H_
#define FONTS_H_

/* reverse:  reverse string s in place */
static void reverse( char s[] );
/* itoa:  convert n to characters in s */
void itoa( long n, char s[] );

typedef enum {small,big} Font;

#endif /* FONTS_H_ */
