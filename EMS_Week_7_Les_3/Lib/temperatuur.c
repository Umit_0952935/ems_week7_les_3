/*
 * temperatuur.c
 *
 *  Created on: 12 mrt. 2018
 *      Author: VersD
 */

#include <stdint.h>
#include <string.h>
#include "i2c.h"
#include "temperatuur.h"
#include "oled.h"
#include "fonts.h"

void initDisplay()
{
    oledInitialize();
    oledSetOrientation(FLIPPED);

    oledClearScreen();

    //Lijn in het midden
    oledFillBox(0,3,127,3,  0x01);
    //Lijn onderaan
    oledFillBox(0,7,127,7,  0x80);
    //lijn rechts
    oledFillBox(127,3,127,7,0xFF);
    //lijn links
    oledFillBox(0,3,0,7,    0xFF);

    oledPrint(60, 4, "Celcius", big);
}

void setTemp(int temp)
{
    //twee char arrays voor de tekst
    char getal[4], decimalen[3];
    uint8_t x=11,y=4;
    uint8_t offset=0;

    //oude cijfers weghalen.
    oledClearBox(11,4,59,6);

    //beperk mogelijkheden
    if(temp>-100 && temp<1000)
    {
        //als negatief
        if(temp < 0)
        {
            //maak positief
            temp*=-1;
            //print een - teken
            offset = oledPrint(x,y, "-", big);
        }

        //tientallen omzetten naar string
        itoa(temp/10,getal);

        //eenheden omzetten naar string
        itoa(temp%10,decimalen);

        //print tientallen . eenheden
        offset += oledPrint(x+offset,   y,  getal,      big);
        offset += oledPrint(x+offset,   y,  ".",        big);
        offset += oledPrint(x+offset,   y,  decimalen,  big);
                  oledPrint(x+offset,   y,  "�",        big);
    }else{
        //anders error
        oledPrint(x,y, "Err-", big);
    }
}

void setTitle(char tekst[])
{
    //print titel
    oledPrint(3,0, tekst, big);
}

/*  aantal hulp functies voor getallen en tekst*/
/* draai array om */
static void reverse( char s[] )
{
    int i, j ;
    char c ;

    //tegelijk optellen en aftellen
    for (i = 0, j = strlen(s)-1; i < j ; i++, j-- )
    {
        c = s[i] ;
        s[i] = s[j] ;
        s[j] = c ;
    }
}

/* itoa:  integer naar ascii omzetting */
void itoa( long n, char s[] )
{
    int i, sign ;

    //als negatief
    if( (sign = n) < 0 )
    {
      n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0';//offset vanaf '0' character
    }while( (n /= 10)>0 );   //delen door 10 en opnieuw

    //als negatief
    if(sign < 0)
    {
        s[i++] = '-'; //voeg - toe
    }

    s[i] = '\0'; //eindigen string

    reverse(s); //draai de boel om
}


