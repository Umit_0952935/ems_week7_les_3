#include <msp430.h> 
int miliseconds;
int distance;
long sensor;

void main(void)
{
  BCSCTL1 = CALBC1_1MHZ;
  DCOCTL = CALDCO_1MHZ;                     // submainclock 1mhz
  WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
  P1DIR |= 1<<1;          // trigger pin as output
  P1DIR &= ~1<<2;         // make pin P1.2 input (ECHO)


  CCTL0 = CCIE;                             // CCR0 interrupt enabled
  CCR0 = 1000;                  // 1ms at 1mhz
  TACTL = TASSEL_2 + MC_1;                  // SMCLK, upmode

  P1IFG  = 0x00;                //clear all interrupt flags
  P1DIR |= 1<<1;                            // P1.0 as output for LED
  P1OUT &= ~1<<1;                           // turn LED off

  __enable_interrupt();       // global interrupt enable

 while(1){
    P1IE &= ~1<<0;          // disable interupt
    P1OUT |= 1<<1;          // generate pulse
    __delay_cycles(10);             // for 10us
    P1OUT &= ~1<<1;                 // stop pulse
    P1IFG = 0x00;                   // clear flag just in case anything happened before
    P1IE |= 1<<2;           // enable interupt on ECHO pin
    P1IES &= ~1<<2;         // rising edge on ECHO pin
    __delay_cycles(30000);          // delay for 30ms (after this time echo times out if there is no object detected)
    distance = sensor/58;           // converting ECHO lenght into cm
    if(distance < 20 && distance != 0){
        __delay_cycles(100000);  //turning LED on if distance is less than 20cm and if distance isn't 0.
        if(distance < 20 && distance != 0)
        {
            __delay_cycles(100000);                 //More if statements just in case of a high frequency out of the environment.
            if(distance < 20 && distance != 0)
            {
                __delay_cycles(100000);
                if(distance < 20 && distance != 0)
                {
                   P1OUT |= 1<<0;
                }
                else
                {
                    P1OUT &= ~1<<0;
                }

            }
            else {
                P1OUT &= ~1<<0;

            }

        }
        else {
            P1OUT &= ~1<<0;
        }

    }



    else P1OUT &= ~1<<0;
 }
}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    if(P1IFG& 1<<2)  //is there interrupt pending?
        {
          if(!(P1IES& 1<<2)) // is this the rising edge?
          {
            TACTL|=TACLR;   // clears timer A
            miliseconds = 0;
            P1IES |= 1<<2;  //falling edge
          }
          else
          {
            sensor = (long)miliseconds*1000 + (long)TAR;    //calculating ECHO lenght

          }
    P1IFG &= ~1<<2;             //clear flag
    }
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  miliseconds++;
}



